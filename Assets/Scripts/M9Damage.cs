﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[System.Serializable]
public class M9Damage : NetworkBehaviour {
	//Player Weapon class

	public string name = "M9";
	int DamageAmount = 5;
	public float TargetDistance;
	public float MaxRange = 15;

	[SerializeField]
	private LayerMask mask; //control what we hit - for example freindly players. 

	
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer) {
			return; //this is stopping th ray cast from being called...
		} else {
			//Fire1 in input controls has a negative button of ctrl- to be removed?
			if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0) {

				Shoot ();

			} else {
				Statistics.timeWithoutShots += Time.deltaTime;
				if (Statistics.timeWithoutShots > 5) {
					Statistics.shotsFired = 0;
				}
			}
		}
	}


	void Shoot()
	{
		RaycastHit shot;
		if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out shot, MaxRange, mask)) {
			//if ray hit then this will be called. 
			TargetDistance = shot.distance;
			Debug.Log ("The shot has hit" + shot.collider.name); 



			/* Was this section made to detect if it is in range? 
			 * 
			 * i changed this: if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out shot))
			 * 
			TargetDistance = shot.distance;
			if (TargetDistance < MaxRange) {
				shot.transform.SendMessage ("RemoveHealth", DamageAmount, SendMessageOptions.DontRequireReceiver);
			}
			*/
		}

		Statistics.timeWithoutShots = 0;
		Statistics.shotsFired += 1;

	}
}
