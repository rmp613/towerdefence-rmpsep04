﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider col){
		if (GlobalAmmo.currentExtraAmmo < GlobalAmmo.maxExtraAmmo) {
			GlobalAmmo.currentExtraAmmo += 10;
			if (GlobalAmmo.currentExtraAmmo > GlobalAmmo.maxExtraAmmo) {
				GlobalAmmo.currentExtraAmmo = GlobalAmmo.maxExtraAmmo;
			}
			this.gameObject.SetActive (false);
		}
	}
}
