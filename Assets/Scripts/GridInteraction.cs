﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridInteraction : MonoBehaviour {
	public class Tower {
		public GameObject tower;
		public int price = 200;
		public Vector3 pos = Vector3.zero;
		public List<Vector3> nodes = new List<Vector3>();

		public Tower(GameObject tower, int price, Vector3 pos){
			this.tower = tower;
			this.price = price;
			this.pos = pos;
		}
		public Tower(GameObject tower, int price){
			this.tower = tower;
			this.price = price;
		}
		public Tower(Tower tower, Vector3 pos){
			this.tower = tower.tower;
			this.pos = tower.pos;
		}
		public Tower (GameObject tower,  Vector3 pos){
			this.tower = tower;
			this.pos = pos;
		}
	}
	Camera mainCamera;
	GameObject wireBox;
	Grid grid;
	public Material wireBoxMat;
	RaycastHit hit;
	List<Vector2> towerNodes;
	GameObject currentBox;
	GridNode currentNode;
	public bool gridInteractionEnabled;
	public enum selectedTowerTypeEnum{ trigun, etc }
	public selectedTowerTypeEnum selectedTowerType = selectedTowerTypeEnum.trigun;
	public List<GameObject> towerPrefabs;
	List<Tower> towers;
	List<Tower> instantiatedTowers;
	List<Tower> storedTowers;
	List<Vector3> worldNodes;
	GameController game;
	Vector3 instanPos= Vector3.zero;
	void TowerSetup(){
		towers = new List<Tower>();
		storedTowers = new List<Tower>();
		instantiatedTowers = new List<Tower>();
		towers.Add (new Tower(towerPrefabs[0], 200, Vector3.zero));
		Debug.Log (towers [0].price);
	}
	// Use this for initialization
	void Start () {
		//temp way of adding prices to towers
		TowerSetup();
		game = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ();
		grid = GetComponent<Grid> ();
		gridInteractionEnabled = false;
		wireBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
		wireBox.GetComponent<MeshRenderer> ().material = wireBoxMat;
		wireBox.GetComponent<BoxCollider> ().enabled = false;
		wireBox.SetActive (false);
		towerNodes = new List<Vector2> ();
		worldNodes = new List<Vector3> ();
	}

	// Update is called once per frame
	void Update () {
		mainCamera = FindCamera();
		if (Input.anyKeyDown) {
			GetInput ();
		}
		if (gridInteractionEnabled) {
			hit = new RaycastHit (); 
			// We need to actually hit an object
			if (
				!Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
				wireBox.SetActive (false);
				return;
			}
			// Check that we are hitting a gameobject with tag Ground
			if (hit.collider.gameObject.tag != "Ground") {
				wireBox.SetActive (false);
				return;
			}
			//If inside grid
			if (grid.GetNode (hit.point).worldPos != new Vector3 (0, Mathf.Infinity, 0)) {
				//towerNodes.Clear ();
				worldNodes.Clear();
				currentNode = grid.GetNode (hit.point);
				//towerNodes.Add (grid.GetVector2(currentNode.worldPos));
				Vector3 offset = hit.point - currentNode.worldPos;
				instanPos = currentNode.worldPos;
				if (offset.x >= 0 && offset.z >= 0) {
					instanPos.x += grid.nodeRadius;
					instanPos.z += grid.nodeRadius;
					//towerNodes.Add (grid.GetVector2(new Vector2(instanPos.x + grid.nodeRadius, instanPos.z)));
					//towerNodes.Add (grid.GetVector2(new Vector2(instanPos.x, instanPos.z + grid.nodeRadius)));
					//towerNodes.Add (grid.GetVector2(new Vector2(instanPos.x + grid.nodeRadius, instanPos.z + grid.nodeRadius)));

				}else if (offset.x >= 0 && offset.z < 0) {
					instanPos.x += grid.nodeRadius;
					instanPos.z -= grid.nodeRadius;
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x + grid.nodeRadius, 0, instanPos.z)));
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x, 0, instanPos.z - grid.nodeRadius)));
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x + grid.nodeRadius, 0, instanPos.z - grid.nodeRadius)));
				}else if (offset.x < 0 && offset.z >= 0) {
					instanPos.x -= grid.nodeRadius;
					instanPos.z += grid.nodeRadius;
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x - grid.nodeRadius, 0, instanPos.z)));
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x, 0, instanPos.z + grid.nodeRadius)));
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x - grid.nodeRadius, 0, instanPos.z + grid.nodeRadius)));

				}else if (offset.x < 0 && offset.z < 0) {
					instanPos.x -= grid.nodeRadius;
					instanPos.z -= grid.nodeRadius;
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x - grid.nodeRadius, 0, instanPos.z)));
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x, 0, instanPos.z - grid.nodeRadius)));
					//towerNodes.Add (grid.GetVector2(new Vector3(instanPos.x - grid.nodeRadius, 0, instanPos.z - grid.nodeRadius)));

				}

				wireBox.SetActive (true);
				wireBox.transform.position = instanPos;
				wireBox.transform.localScale = new Vector3 (grid.nodeRadius * 4, .3f, grid.nodeRadius * 4);
				Vector3 nodePos = Vector3.zero;
				nodePos.x = instanPos.x + grid.nodeRadius;
				nodePos.z = instanPos.z + grid.nodeRadius;
				worldNodes.Add (nodePos);
				nodePos.x = instanPos.x + grid.nodeRadius;
				nodePos.z = instanPos.z - grid.nodeRadius;
				worldNodes.Add (nodePos);
				nodePos.x = instanPos.x - grid.nodeRadius;
				nodePos.z = instanPos.z + grid.nodeRadius;
				worldNodes.Add (nodePos);
				nodePos.x = instanPos.x - grid.nodeRadius;
				nodePos.z = instanPos.z - grid.nodeRadius;
				worldNodes.Add (nodePos);
			}
		}
	}
	private Camera FindCamera()
	{
		if (GetComponent<Camera>())
		{
			return GetComponent<Camera>();
		}

		return Camera.main;
	}
	private void GetInput(){
		if (GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().state != GameController.GameState.PHASE_BUILDING) {
			wireBox.SetActive (false);
			gridInteractionEnabled = false;	


			return;

		}
		if (Input.GetKeyDown (KeyCode.I)) {
			gridInteractionEnabled = !gridInteractionEnabled;			
			wireBox.SetActive (!wireBox.activeSelf);

		}
		if (gridInteractionEnabled) {
			if (Input.GetKeyDown ("1")) {
				selectedTowerType = selectedTowerTypeEnum.trigun;
			} else if (Input.GetKeyDown ("2")) {
				selectedTowerType = selectedTowerTypeEnum.etc;
			}
			if (Input.GetMouseButtonDown (0) && currentNode != null) {

				if (selectedTowerType == selectedTowerTypeEnum.trigun) {
					bool canBuildHere = true;
					foreach (Vector3 wn in worldNodes) {
						if (!grid.GetNode (wn).walkable) {
							canBuildHere = false;
							//Debug.Log ("cant");
							return;
						}
					}
					
					if (canBuildHere && game.funds >= towers [0].price) {
						Debug.Log ("cant");
						GameObject newTowerObj = (GameObject)GameObject.Instantiate (towers [0].tower, instanPos, Quaternion.identity);
						Tower newTower = new Tower (newTowerObj, instanPos);
						instantiatedTowers.Add (newTower);
						List<Vector3> newNodes = worldNodes;
						instantiatedTowers[instantiatedTowers.Count-1].nodes = newNodes;
						game.funds -= 200;
						foreach (Vector3 wn in worldNodes) {
							grid.GetNode (wn).walkable = false;
						}
					}	
				}
			}

			if (Input.GetMouseButtonDown (1)) {
				if (Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
					if (hit.collider.gameObject.tag == "Tower") {
						foreach (Tower tower in instantiatedTowers) {
							if (tower.tower == hit.collider.gameObject) {
								game.funds += tower.price / 2;
								foreach (Vector3 tn in tower.nodes) {
									grid.GetNode (tn).walkable = true;
								}
								Destroy (tower.tower);
								instantiatedTowers.Remove (tower);
							}
						}
					}
				}
			}
			if (Input.GetKeyDown (KeyCode.C)) {
				if (Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
					if (hit.collider.gameObject.tag == "Tower") {
						foreach (Tower tower in instantiatedTowers) {
							if (tower.tower == hit.collider.gameObject) {

							}
						}
					}
				}
			}
		}
	}
}
